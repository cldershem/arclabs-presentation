# Arc Labs
- Initial Idea: To provide __low cost__, __low power__, and __long range__ wireless connectivity for IoT devices
- Size of Opportunity:
    - TAM (World): $1.01 Billion
    - SAM (USA): $250 million
    - TM (Indianapolis): $1.5 million

--##--
## Team
- ### Aaron Dershem
    - System Administrator
    - 12 years of experience in IT and SaaS products

- ### Ronald Pulliam
    - Electrical Engineer, MBA
    - 14 years Engineering and Business experience

- ### Cameron Dershem
    - Software Developer
    - 5 years development experience

--##--
- ## Problem
    - Data informs decisions
    - Data is difficult and expensive to collect
    - People make up numbers
- ## Opportunity
    - IoT can solve this, but it's expensive and difficult
- ## Original Plan
    - Provide a low cost, low power, long range network to enable people to make IoT (the Internet of Things) happen

--##--
<img src="assets/Week2.jpg">
Note: Discuss the boiling of the ocean problem

--##--
## Weeks 1-3: Interviews
- We tried to get 10 interviews in by doing 3-4 a piece
- 1-2 interviews a week on big ticket customer segments
- Use pet owners as filler interviews
    - We'd hoped to rule out pet trackers as a business segment

--##--
## Weeks 1-3: Conclusions
- Difficult to sit down with the right people
- Not enough time to get quality interviews
- Quantity over quality isn't a good way to live
- Manufacturing and industrial have a lot of problems
    - We aren't the solution right now
    - IoT might be an answer in the future
- Technologists (read: nerds) were interested and excited, but failed to identify must haves or needs
- Some people really shouldn't own pets

--##--
<img src="assets/Week3.jpg">

--##--
<img src="assets/Week4.jpg">
Note: Able to rule out manufacturing. Pets still 50/50

--##--
## Week 4-7: Giving Up
- Ruled out Manufacturing and Industrial
- Decided to focus on agriculture and municipalities
- Decided to go our own pace and quit focusing on quantity
- Went to GIS Day and IoT Day

--##--
## Week 4-7: Conclusions
- Don't try to meet with Farmers in the Fall
- GIS community has a lot of needs
    - IoT as a concept is not even on their radar
- We realized before we can sell someone an IoT solution we need to educate about what and why IoT is

--##--
<img src="assets/Week5.jpg">

--##--
## UPDATE business model canvas and insert it here.
<img src="assets/Week9.jpg">

--##--
## Week 8: Post Giving Up
- Slowed the pace even further and went back to our original ideas
- Discussed what inspired the whole business plan in the first place
- Went to a hackathon on a whim
    - Asked to make an appearance as representatives of the Indy Tech community
    - Thought we might be able to help another group

--##--
## Week 8: Post giving up - Conclusions
- Least likely candidates might turn out to be your biggest champion
- Pupil dilation never gets old (even if exaggerated by coffee and redbull)
- Cities ~~might~~ actually ~~want~~ need this
- First time we had people __get__ us
    - Other people who'd just learned of the idea started telling others about it
    - People sought us out
    - Unsolicited problems to be solved coming from left and right

--##--
## Key Assumptions
- Better to know than to guess
- Low cost, low power, long range is something people want
- People have heard of the internet

--##--
## Key Lessons Learned
- Getting out of the building works, but remember quality is what matters
- Don't lose focus of why you started
- One data point isn't enough to base a decision on
- 50% of Pet Owner's are terrifying
- Neal's camel's nose fable isn't that crazy after all

--##--
<video data-autoplay src="assets/MVI_4351.MOV" muted="true"></video>

--##--
- ## Is this a viable business?
    - Yes.
- ## Will we continue to pursue it?
    - We're planning an "What is IoT?" event to educate
    - We're helping organize an IoT hackathon to promote innovation

--##--
Appendix: All Business Model Canvas

--##--
<img src="assets/Week2.jpg">

--##--
<img src="assets/Week3.jpg">

--##--
<img src="assets/Week4.jpg">

--##--
<img src="assets/Week5.jpg">

--##--
<img src="assets/Week9.jpg">
