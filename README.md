## EDIT
- slides are in `slides.md`

## INSTALL
```
git clone git@gitlab.com:cldershem/arclabs-presentation.git
npm install
npm start
```

## DEPLOY
- Not working yet
- `git push -u origin master` in theory should trigger a build
- should be deployed at 'http://cldershem.gitlab.io/arclabs-presentation'
